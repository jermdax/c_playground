#include <stdio.h>

typedef char (*bool_fp)();

#define TEST(name) __attribute__((section("asd"))) bool_fp testfp_##name = name
extern bool_fp __start_asd; // linker will magically create this
extern bool_fp __stop_asd; // and this

char test_fp()
{
	return 1;
}

char test1_fp()
{
	return 0;
}

// Here's the magic
TEST(test_fp);
TEST(test1_fp);

int main(int argc, char* argv[])
{
	printf("Sec start is: %x\n", &__start_asd);
	printf("Sec end is: %x\n", &__stop_asd);
	// Get the function addresses by name
	printf("addr of test_fp : %x\n", test_fp);
	printf("addr of test1_fp: %x\n", test1_fp);
	// Get the function addresses indirectly
	bool_fp *iter = &__start_asd;
	for(iter = &__start_asd; iter != &__stop_asd; ++iter)
	{
		printf("addr of func pointer in section: %x\n", *iter);
	}
	return 0;
}
